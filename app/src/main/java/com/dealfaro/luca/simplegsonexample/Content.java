package com.dealfaro.luca.simplegsonexample;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by luca on 26/1/2016.
 */

//@Generated("org.jsonschema2pojo")
public class Content {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("score")
    @Expose
    public Integer score;
    @SerializedName("badges")
    @Expose
    public List<String> badges = new ArrayList<String>();

}

