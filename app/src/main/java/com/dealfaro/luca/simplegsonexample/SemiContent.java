package com.dealfaro.luca.simplegsonexample;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luca on 26/1/2016.
 */

//@Generated("org.jsonschema2pojo")
public class SemiContent {

    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("score")
    @Expose
    public Integer score;
    @SerializedName("address")
    @Expose
    public String address;

}

