package com.dealfaro.luca.simplegsonexample;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;

import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "json_example";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void clickButton(View v) {
        // Creates some fake data.
        Content c = new Content();
        c.name = "Luca";
        c.score = 32;
        c.badges.add("Beginner");
        c.badges.add("Learning");
        // Turns it into a string.
        Gson gson = new Gson();
        String s = gson.toJson(c);
        // Let's log it.
        Log.i(LOG_TAG, "Json string: " + s);
        // And let's write it to the settings.
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor e = prefs.edit();
        e.putString("myprefs", s);
        e.commit();
        Intent intent = new Intent(this, SecondActivity.class);
        startActivity(intent);
    }



}
